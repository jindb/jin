include("jin.jl")
include("config.jl")

using Sockets

server = listen(CONFIG[:port])

while true
  conn = accept(server)
  @async begin
    try
      while true
        line = readline(conn)
        output = parse(line)
        write(conn, "$output\n")
      end
    catch err
      print("connection ended with error $err")
    end
  end
end
