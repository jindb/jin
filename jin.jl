include("crd.jl")

function parse(string::String)
    splitted_string = split(string, " ")
    command = splitted_string[1]

    if command == "set"
        symbol = splitted_string[2]
        value = join(splitted_string[3:length(splitted_string)], " ")
        set(symbol, value)
    elseif command == "get"
        symbol = splitted_string[2]
        get(symbol)
    elseif command == "del"
        symbol = splitted_string[2]
        del(symbol)
    else
        ""
    end
end
