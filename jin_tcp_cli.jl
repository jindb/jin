include("config.jl")

using Sockets

keep_going = true

clientside = connect(CONFIG[:host], CONFIG[:port])

while keep_going
    print("jin> ")
    string = readline()

    if strip(string) == "exit"
        global keep_going = false
        close(clientside)
    else
        println(clientside, string)
        print(readline(clientside, keep=true))
    end
end
