# crd.jl
# Creat read and delete library

dictionary = Dict()

function set(symbol, value)
    dictionary[Symbol(symbol)] = value
    return "OK"
end

function get(symbol)
    try
        return dictionary[Symbol(symbol)]
    catch
        return ""
    end
end

function del(symbol)
    delete!(dictionary, Symbol(symbol))
    return "OK"
end
