include("jin.jl")

keep_going = true

while keep_going
    print("jin> ")
    string = readline()

    if strip(string) == "exit"
        global keep_going = false
    else
        println(parse(string))
    end
end
