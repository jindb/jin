# jin DB

![](https://bafybeia434v6gev3kbghcazs3hdidrvk2vsmeor7mge3igpf3k6dgtt3gq.ipfs.dweb.link/jin%20store%20logo.svg)

jin DB or Julia In Memory Data Base is a key value store application which you can use to store very simple key value pairs. This app can be used for communication between server boxes and as a cache in secure intranets.

## Getting Started

Get Julia installed. Clone this repo. Start the server `julia jin_server.jl`, in another terminal(s) start the cli using `julia jin_tcp_cli.jl`. Press `Ctrl + C` to terminate.

In TCP CLI, to set a value use:

```
jin> set a 20
```

to get a value:

```
jin> get a
```

to delete a value:

```
jin> del a
```

to exit

```
jin> exit
```

## Future Plan

* [] Develop tests.
* [] Develop a Julia package to connect with jin.
* [] Get insanely rich.
* [] Destroy Earth.
* [] Send humans to Mars claiming Earth is getting destroyed.

## License

jin is released under [GPLV3](https://www.gnu.org/licenses/gpl-3.0.en.html) or later.
